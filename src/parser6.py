import sys, random
from constants import *
from operations import *
parsefile = [
    './cw6_271_BLACK.txt',
    './cw6_271_WHITE.txt',
    ]
for file in parsefile:
    f1=open(file, 'r')
    with f1:
        content = f1.readlines()
    f1.close()
    mp = {}
    for s in content:
        ss = s.split()
        ID = (int(ss[0]),int(ss[1]),int(ss[2]))
        score = float(ss[3])
        times = int(ss[4])
        if ID not in mp:
            mp[ID] = (0.0,0)
        mp[ID] = (mp[ID][0]+score*times,mp[ID][1]+times)
    lt = []
    for i in mp:
        if mp[i][1]!=0:
            lt.append((i,mp[i][0]/mp[i][1],mp[i][1]))
    lt.sort()
    f1=open(file, 'w')
    for i in lt:
        print >>f1, str(i[0][0])+' '+str(i[0][1])+' '+str(i[0][2])+' '+str(i[1])+' '+str(i[2])
    f1.close()