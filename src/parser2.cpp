#include<cstdio>
#include<map>
#include<set>
#include<vector>
#include<cassert>
#include<algorithm>
using namespace std;
typedef unsigned long long int unt;
typedef double dou;
typedef pair<dou,int> P;
set<vector<unt>>mp;
int myget(vector<unt>&a,int n){
	a.clear();
	for(int i=0;i<n;i++){
		unt x;
		if(scanf("%I64u",&x)==EOF)return 0;
		getchar();
		a.push_back(x);
	}
	return 1;
}
char fn[10][100]={"cw6_node.txt","cw6_edge.txt","count_win_Yuehs_271_WHITE.txt"};
char tn[10][100]={"cw6_node.txt","cw6_edge.txt","count_win_Yuehs_271_WHITE.txt"};
int main(){
	freopen(fn[0],"r",stdin);
	mp.clear();
	vector<unt>a;
	for(;myget(a,4);){
		mp.insert(a);
	}
	freopen(tn[0],"w",stdout);
	for(auto&it:mp){
		int n = (int)it.size();
		for(int j=0;j<n;j++){
			printf("%I64u%c",it[j],j==n-1?10:32);
		}
	}
	//////////////
	freopen(fn[1],"r",stdin);
	mp.clear();
	char s[5000];
	char*ts;
	for(;(*s=getchar())!=EOF;){
		gets(s+1);
		ts=s;
		a.clear();
		for(;*ts;){
			for(;*ts&&('0'<=*ts&&*ts<='9')==0;ts++);
			if(*ts==0)break;
			unt tmp=0;
			for(;*ts&&('0'<=*ts&&*ts<='9')!=0;ts++){
				tmp=tmp*10+(*ts)-'0';
			}
			a.push_back(tmp);
		}
		if(a.size()>=3)sort(a.begin()+1,a.end());
		mp.insert(a);
	}
	freopen(tn[1],"w",stdout);
	for(auto&it:mp){
		int n = (int)it.size();
		for(int j=0;j<n;j++){
			printf("%I64u%c",it[j],j==n-1?10:32);
		}
	}	
	return 0;
}

