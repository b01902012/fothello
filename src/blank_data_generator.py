from constants import *
from array import array
def resetBoard(board):
    for x in range(8):
        for y in range(8):
            board[x][y] = NONE

    board[3][3] = BLACK
    board[3][4] = WHITE
    board[4][3] = WHITE
    board[4][4] = BLACK

def getNewBoard():
    board = []
    for i in range(8):
        board.append(array('i',[NONE] * 8))
    return board

def getBoardID_6(board, myTile):
    ID = []
    for i in range(3):
        ID.append([0]*8)
    for x in range(8):
        for y in range(8):
            ID[board[x][y]][0]+= 1<<(00+x*8+y)
            ID[board[x][y]][1]+= 1<<(07+x*8-y)
            ID[board[x][y]][2]+= 1<<(63-x*8-y)
            ID[board[x][y]][3]+= 1<<(56-x*8+y)
            ID[board[x][y]][4]+= 1<<(00+y*8+x)
            ID[board[x][y]][5]+= 1<<(07+y*8-x)
            ID[board[x][y]][6]+= 1<<(63-y*8-x)
            ID[board[x][y]][7]+= 1<<(56-y*8+x)
    ans = (ID[0][0],ID[1][0],myTile)
    for i in range(8):
        ans = min(ans,(ID[0][i],ID[1][i],myTile))
    return ans

def isOnBoard(x, y):
    return x >= 0 and x <= 7 and y >= 0 and y <=7

def isValidMoveBool(board, myTile, xstart, ystart):
    opTile = myTile^1
    for dx, dy in [ [0, 1], [1, 1], [1, 0], [1, -1], [0, -1], [-1, -1], [-1, 0], [-1, 1] ]:
        x, y = xstart+dx, ystart+dy
        l = 0
        while isOnBoard(x, y) and board[x][y] == opTile:
            l = 1
            x += dx
            y += dy
        if l and isOnBoard(x, y) == True and board[x][y] == myTile:
            return True
    return False

def getTileToFlip(board, myTile, xstart, ystart):
    opTile = myTile^1
    tilesToFlip=[]
    for dx, dy in [ [0, 1], [1, 1], [1, 0], [1, -1], [0, -1], [-1, -1], [-1, 0], [-1, 1] ]:
        x, y = xstart+dx, ystart+dy
        l = 0
        while isOnBoard(x, y) and board[x][y] == opTile:
            l += 1
            x += dx
            y += dy
        if l==0 or isOnBoard(x, y) == False or board[x][y] != myTile:
            continue
        while l>0:
            l-=1
            x-=dx
            y-=dy
            tilesToFlip.append([x,y])
    return tilesToFlip

global gBVM_lg
gBVM_lg = array('i',[0]*(1<<16))
for i in range(16):
    gBVM_lg[1<<i] = i
def getBothValidMoves_3(board, myTile, restnone):
    global gBVM_lg
    Moves = []
    opMoveslen = 0
    for k in range(4):
        mask = ( ( restnone>>(k<<4) )&65535 )
        while mask!=0:
            lowbit = mask - ( mask&(mask-1) )
            xy = gBVM_lg[lowbit] + (k<<4)
            x, y = xy>>3, xy&7
            mask-= lowbit
            if isValidMoveBool(board, myTile, x, y) == True:
                Moves.append([x, y])
            opMoveslen = opMoveslen or isValidMoveBool(board, myTile^1, x, y)
    return Moves, opMoveslen
global cnt1
global cnt2
global cnt3
global isVis
cnt1, cnt2, cnt3 = 0, 0, 0
isVis = {}
global IDT
global NID
IDT = 0
NID = {}
def dfs_8(board, myTile, restnone, depth, ID):
    global cnt1
    global cnt2
    global cnt3
    global IDT
    global NID
    if depth == 0 :
        cnt1+=1
        if (cnt1&65535)==0:
            print cnt1
        NID[ID]=IDT
        IDT+=1
        return None
    opTile = myTile^1
    myMoves, opMoveslen = getBothValidMoves_3(board, myTile, restnone)
    if len(myMoves) == 0:
        if opMoveslen == 0:
            cnt2+=1
            print 'jizz'
        else:
            cnt3+=1
            print 'loli'
            ###################################
            nID = getBoardID_6(board, opTile)
            if not nID in isVis:
                isVis[nID] = []
                dfs_8(board, opTile, restnone, depth-1, nID)
            isVis[ID].append(nID)
            ###################################
            NID[ID]=IDT
            IDT+=1
        return None
    for move in myMoves:
        tilesToFlip = getTileToFlip(board, myTile, move[0], move[1])
        ###################################
        mid = (move[0]<<3)+move[1]
        board[move[0]][move[1]] = myTile
        for x ,y in tilesToFlip:
            board[x][y]^=1
        restnone-= 1 << mid
        ###################################
        nID = getBoardID_6(board, opTile)
        if not nID in isVis:
            isVis[nID] = []
            dfs_8(board, opTile, restnone, depth-1, nID)
        isVis[ID].append(nID)
        ###################################
        board[move[0]][move[1]] = NONE
        for x ,y in tilesToFlip:
            board[x][y]^=1
        restnone+= 1 << mid
        ###################################
    NID[ID]=IDT
    IDT+=1
    return None
board = getNewBoard()
resetBoard(board)
cnt = 0
restnone = 0
for xy in range(64):
    cnt+= ( board[xy>>3][xy&7] == NONE )
    restnone+= ( board[xy>>3][xy&7] == NONE ) << xy
oID = getBoardID_6(board, BLACK)
isVis[oID] = []
dfs_8(board, BLACK, restnone, 7, oID)

# f1 = open('cw6_node.txt','w')
# f2 = open('cw6_edge.txt','w')
b = []
for i in isVis:
    for j in range(len(isVis[i])):
        isVis[i][j] = NID[isVis[i][j]]
    isVis[i] = set(isVis[i])
    isVis[i] = list(isVis[i])
    isVis[i].sort()
    b.append((NID[i],i,isVis[i]))
b.sort()
isVis = []
ES=0
# for ID in b:
    # print >>f1, str(ID[0])+' '+str(ID[1][0])+' '+str(ID[1][1])+' '+str(ID[1][2])
for ID in b:
    ES+=len(ID[2])
    # if len(ID[2])>0:
        # s = str(ID[0])
        # for TO in ID[2]:
            # s+= ' '+str(TO)
        # print >>f2, s
print 'V = ',len(b),',E = ',ES
print cnt1
print cnt2
print cnt3