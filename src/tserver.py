import time
import socket
import BaseHTTPServer
from constants import *
from operations import *
from players_noGUI import *
###################################################################
player = []
def addP(AI, grader, f, ab, aw, s, x):
    tp = SetPlayer(AI)
    tp.player.setgrader(grader)
    tp.player.setfBound(f)
    tp.player.setadvise(ab,BLACK)
    tp.player.setadvise(aw,WHITE)
    tp.player.setsBound(s)
    tp.player.setxLevel(x)
    player.append(tp)
    return len(player)-1
###################################################################
advisor0 = Fisrt8AI(None)
advisor1 = Fisrt8AI('./count_win.txt')
advisor2 = Fisrt8AI('./count_win_Yuehs_271_BLACK.txt')
advisor3 = Fisrt8AI('./count_win_Yuehs_271_WHITE.txt')
advisor4 = Fisrt6AI('./cw6_271_BLACK.txt')
advisor5 = Fisrt6AI('./cw6_271_WHITE.txt')
advisor6 = Fisrt8AI('./count_win_anti_271_BLACK.txt')
advisor7 = Fisrt8AI('./count_win_anti_271_WHITE.txt')
###################################################################
player.append(0)#0
addP('sauce',Dalu()     ,8,advisor1,advisor1,13,2)#1
addP('sauce',Yuehs()    ,8,advisor1,advisor1,13,2)#2
addP('sauce',Yuehs_2()  ,8,advisor1,advisor1,13,2)#3
addP('sauce',Yuehs_27() ,8,advisor1,advisor1,13,2)#4
addP('sauce',Yuehs_271(),8,advisor2,advisor3,13,2)#5 regular
addP('sauce',Kart()     ,8,advisor1,advisor1,13,2)#6
addP('sauce',Kart_2()   ,8,advisor1,advisor1,13,2)#7
addP('sauce',Yuehs_271(),8,advisor1,advisor1,13,2)#8 anti
addP('sauce',Kart()     ,8,advisor6,advisor7,13,2)#9
addP('sauce',Kart_2()   ,8,advisor6,advisor7,13,2)#10
addP('sauce',Yuehs_271(),0,advisor0,advisor0,13,2)#11 no advisor
addP('sauce',Kart()     ,0,advisor0,advisor0,13,2)#12
addP('sauce',Kart_2()   ,0,advisor0,advisor0,13,2)#13
addP('sauce',Yuehs_271(),8,advisor2,advisor3,14,4)#14 deeper xLevel
addP('sauce',Kart()     ,8,advisor1,advisor1,14,4)#15
addP('sauce',Kart_2()   ,8,advisor1,advisor1,14,4)#16
addP('sauce',Yuehs_271(),8,advisor2,advisor3,-0,2)#17 no Endgame
addP('sauce',Kart()     ,8,advisor1,advisor1,-0,2)#18
addP('sauce',Kart_2()   ,8,advisor1,advisor1,-0,2)#19
###################################################################

HOST_NAME = socket.gethostname()
PORT_NUMBER = 12711 # Maybe set this to 9000.

print socket.gethostbyname(socket.gethostname())

def IDToBoard(ID):
    board = getNewBoard()
    for x in range(8):
        for y in range(8):
            tmp = (ID[BLACK]>>(x*8+y))%2*2+(ID[WHITE]>>(x*8+y))%2
            if tmp == 0:
                board[x][y] = NONE
            elif tmp == 1:
                board[x][y] = WHITE
            elif tmp == 2:
                board[x][y] = BLACK
            else:
                return None, None
    return board, ID[2]

def boardHtml(board, myTile):
    Moves = getBothValidMoves(board)
    
    tmps = ''
    for x in range(8):
        for y in range(8):
            if board[x][y] == NONE:
                if [x, y] in Moves[myTile]:
                    tileToFlip = getTileToFlip(board, myTile, x, y)
                    #########################
                    for tx, ty in tileToFlip:
                        board[tx][ty]^= 1
                    board[x][y] = myTile
                    #########################
                    ID = getBoardID_2(board, myTile^1)
                    tID = (ID[0]<<65)+(ID[1]<<1)+ID[2]
                    #########################
                    for tx, ty in tileToFlip:
                        board[tx][ty]^= 1
                    board[x][y] = NONE
                    #########################
                    tmps+= "<a href=\"http://linux10.csie.ntu.edu.tw:12711/" + str(tID) + "\">"
                    tmps+= "<img src=\"http://www.csie.ntu.edu.tw/~b01902012/none.png\"></a>"
                else:
                    tmps+= "<img src=\"http://www.csie.ntu.edu.tw/~b01902012/none.png\"></a>"
            elif board[x][y] == BLACK:
                tmps+= "<img src=\"http://www.csie.ntu.edu.tw/~b01902012/black_2.png\"></a>"
            elif board[x][y] == WHITE:
                tmps+= "<img src=\"http://www.csie.ntu.edu.tw/~b01902012/white_2.png\"></a>"
        tmps+= "<br>"
    return tmps

class MyHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_HEAD(s):
        s.send_response(200)
        s.send_header("Content-type", "text/html")
        s.end_headers()
    def do_POST(s):
        s.send_response(200)
        s.send_header("Content-type", "text/html")
        s.end_headers()
    def do_GET(s):
        """Respond to a GET request."""
        s.send_response(200)
        s.send_header("Content-type", "text/html")
        s.end_headers()
        s.wfile.write("<html>")
        s.wfile.write("<head>")
        s.wfile.write("<title>othello</title>")
        s.wfile.write("</head>")
        s.wfile.write("<body>")
        s.wfile.write(s.path)
        s.wfile.write("<br>")
        
        path = s.path[1:]
        flag = 0
        if path.isdigit():
            #tID = decrpt(long(path))
            tID = long(path)
            ID = [tID>>65,(tID>>1)&((1<<64)-1),tID&1]
            board, myTile = IDToBoard(ID)
            flag = board != None
        if flag == 0:
            board = getNewBoard()
            resetBoard(board)
            myTile = BLACK
        else:
            Moves = getBothValidMoves(board)
            cnt = 0
            while len(Moves[myTile]) and (cnt == 0 or len(Moves[myTile^1]) == 0):
                x, y = player[14].player.getMove(board, myTile)
                tileToFlip = getTileToFlip(board, myTile, x, y)
                #########################
                for tx, ty in tileToFlip:
                    board[tx][ty]^= 1
                board[x][y] = myTile
                #########################
                cnt = 1
                Moves = getBothValidMoves(board)
            myTile^= 1
            if len(Moves[myTile]) == 0 and len(Moves[myTile^1]) == 0:
                score = getScoreOfBoard(board)
                s.wfile.write(str(score[0])+':'+str(score[1])+"<br>")
                if score[0] > score[1]:
                    s.wfile.write("Congratulation! You beats me!<br>")
                elif score[0] == score[1]:
                    s.wfile.write("Not bad.<br>")
                elif score[0] + 20 <= score[1]:
                    s.wfile.write("Easy win!<br>")
                elif score[0] < score[1]:
                    s.wfile.write("I win!<br>")
                else:
                    s.wfile.write("Error<br>")
        s.wfile.write(boardHtml(board, myTile))
        s.wfile.write("</body>")
        s.wfile.write("</html>")

if __name__ == '__main__':
    server_class = BaseHTTPServer.HTTPServer
    httpd = server_class((HOST_NAME, PORT_NUMBER), MyHandler)
    print time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER)