def getrcc(A):
    cc = 0
    for i in range(8):
        if A[i]&4:
            cc = cc*6+(A[i]^1)
        else:
            cc = cc*6+(A[i]>>1)+((A[i]&1)<<1)
    return cc

def putA(A, pos):
    nA=A
    nA[pos]=4
    x, l = pos+1, 0
    while x<=7 and nA[x]==5:
        l+= 1
        x+= 1
    if x<=7 and nA[x]==4:
        while l!=0:
            l-= 1
            x-= 1
            nA[x] = 4
    x, l = pos-1, 0
    while x>=0 and nA[x]==5:
        l+= 1
        x-= 1
    if x>=0 and nA[x]==4:
        while l!=0:
            l-= 1
            x+= 1
            nA[x] = 4
    flag1 = pos+1<=7 and (nA[pos+1]==0 or nA[pos+1]==1)
    flag2 = pos-1>=0 and (nA[pos-1]==0 or nA[pos-1]==1)
    if flag1 and flag2:
        if pos+1==7:
            flag1=1
        elif pos-1==0:
            flag1=1
        else:
            flag1=1
    elif flag1:
        if pos+1==7:
            pp= 1.0/2.0
        else:
            pp= 2.0/3.0
        ans=[]
        ans.append((1.0-pp,getrcc(nA)));
        nA[pos-1]+=2
        ans.append((0.0+pp,getrcc(nA)));
        return ans
    elif flag2:
        if pos-1==0:
            pp= 1.0/2.0
        else:
            pp= 2.0/3.0
        ans=[]
        ans.append((1.0-pp,getrcc(nA)));
        nA[pos-1]+=2
        ans.append((0.0+pp,getrcc(nA)));
        return ans
    return [(1.0,getrcc(nA))]
        
def make_DP(DP, cc):
    if DP[cc] != -7122:
        return DP[cc]
    A =array('i',[0]*8)
    mC, oC=0, 0
    mM, oM=[], []
    for i in range(8):
        nc = cc/6
        A[i] = cc-nc*6
        cc = nc
        if A[i]&4:
            mC+=(A[i]&1)==0;
            oC+=(A[i]&1)==1;
        else:
            if A[i]&1:
                mM.append(i)
            if A[i]&2:
                oM.append(i)