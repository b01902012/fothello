class Fisrt7AI:
    ################################build_node
    print 'start build node'
    startloadtime = time.clock()
    f1=open('count_win_node.txt','r')
    with f1:
        content = f1.readlines()
    f1.close()
    NID = {}
    for s in content:
        ss = s.split()
        ID = (int(ss[1]),int(ss[2]),int(ss[3]))
        NID[ID] = int(ss[0])
    IDT = len(NID)
    RID = [None] * IDT
    for i in NID:
        RID[NID[i]] = i
    print 'end build node: ', time.clock()-startloadtime
    ################################build_edge
    print 'start build edge'
    startloadtime = time.clock()
    print 'start load edge'
    f1=open('count_win_edge.txt','r')
    with f1:
        content = f1.readlines()
    f1.close()
    print 'end load edge: ', time.clock()-startloadtime
    
    print len(content), len(NID)
    EDG = [None] * IDT
    for s in content:
        ss = s.split()
        From = int(ss[0])
        Tolist = []
        for To in range(1,len(ss),1):
            Tolist.append(int(ss[To]))
        EDG[From] = array('i',Tolist)
    print 'end build edge', time.clock()-startloadtime
    ################################
    def __init__(self):
        self.DPA=[]
    def make_DP(self, j):
        if self.DPA[j][0]!=2:
            return None
        if len(self.EDG[j]) == 0:
            self.DPA[j]=(0,[])
            return None
        tmp = 2.0
        tmv = []
        for k in self.EDG[j]:
            self.make_DP(k)
            if tmp > self.DPA[k][0]:
                tmp = self.DPA[k][0]
                tmv = []
                tmv.append(k)
            elif tmp == self.DPA[k][0]:
                tmv.append(k)
        self.DPA[j]=(-tmp,tmv)
        return None
    def loadFile(self, file):
        print 'Fisrt8AI start loading ', file
        starttime = time.clock()
        self.DPA = [(2,[])] * self.IDT
        f1=open(file, 'r')
        with f1:
            content = f1.readlines()
        f1.close()
        for s in content:
            ss = s.split()
            ID = (int(ss[0]),int(ss[1]),int(ss[2]))
            if ID in self.NID:
                self.DPA[self.NID[ID]]=(float(ss[4]),[])
        for i in range(self.IDT):
            self.make_DP(i)
        print 'F8 loadFile time: ', time.clock()-starttime
    def isMove(self, board, Tile):
        ID = getBoardID(board, Tile)
        return ID in self.NID and len(self.DPA[self.NID[ID]][1])>0
    def getMove(self, board, Tile):
        if self.isMove(board, Tile):
            #print 'get from fisrt8'
            ID = getBoardID(board, Tile)
            ID = self.RID[ random.choice(self.DPA[self.NID[ID]][1]) ]
            if Tile == BLACK:
                myMoves, opMoves = getBothValidMoves(board)
            else:
                opMoves, myMoves = getBothValidMoves(board)
            for move in myMoves:
                nboard = getBoardCopy(board)
                makeMove(nboard, Tile, move[0], move[1])
                if ID == getBoardID(nboard, Tile^1):
                    return move
            print 'fisrt8 getMove Error!'
            return (0, 0)
        else:
            return EndAllWithMinMaxAI().getMove(board, Tile)